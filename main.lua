-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

libConfig = require('libConfig')
require('lib.libUtils')

local myTable = {
	name = 'Jon',
	age = 44
}

print_r(myTable)

print('Table count of elements: ' .. count(myTable))

if isNumber(myTable) then
	print('Is Number: ' .. isNumber(myTable))
else
	print('myTable is not number')
end

if isTable(myTable) then
	print('myTable is table')
end

print(getItem(myTable, 'age', -1))
print(getItem(myTable, 'age_not_exist', -1))
