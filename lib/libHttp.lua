----------------------------------------------
--
--	libHtpp.lua
--
----------------------------------------------
--	This library work with HTTP
--	Required libraries: libUtils(global), libUrl

local libConfig = require('libconfig')
local libUrl = require(libConfig.path .. 'libUrl')
local json = require('json')

local M = {}

M.codes = {
	EMPTY_URL = "EMPTY_URL",
	HEADERS_MUST_BE_TABLE = "HEADERS_MUST_BE_TABLE",
	BODY_MUST_BE_TABLE = "BODY_MUST_BE_TABLE"
}

--[====[
	@type func
	@name libHttp.request(obj)
	@param obj:table Settings for request
	@return string/nil If have error that return string, if not error return nil
	@brief Get input settings and send request
	@description [
		Input `obj` need be table. All possible configuration below:
		<pre>
		{
			method = 'GET;POST...', -- optional. Default value - `GET`
			dataType = 'text/plain;application/json', -- optional. Default value `text/plain`. If `application/json` it parse and return table
			handler = function, -- optional. Default empty function. Handler call after request
			headers = table, -- optional. Default `Content-Type: application/x-www-form-urlencoded`. Can be set any headers
			body = table, -- optional. Default empty. You can transfer any data that will be send with request
		}
		</pre>
	]
--]====]
M.request = function(obj)
	local url = getItem(obj, 'url', nil)

	--	URL can't be empty
	if empty(url) then
		return M.codes.EMPTY_URL
	end

	local method = getItem(obj, 'method', 'GET')
	local dataType = getItem(obj, 'dataType', 'text/plain')
	local handler = getItem(obj, 'handler', nil)
	local headers = getItem(obj, 'headers', nil)

	if empty(handler) then
		handler = function() end
	end

	--	headers con't be empty and that set default value
	if empty(headers) then
		headers = {}
		headers['Content-Type'] = 'application/x-www-form-urlencoded'
	end


	--	headers need be table
	if not isTable(headers) then
		return M.codes.HEADERS_MUST_BE_TABLE
	end

	local body = getItem(obj, 'body', {})

	--	If get body than it need be table
	if not empty(body) and not isTable(body) then
		return M.codes.BODY_MUST_BE_TABLE
	end

	body = libUrl.getQueryParams(body)

	local params = {
		headers = headers,
		body = body
	}

	--	Send request and call handler
	network.request(url, method, function(e)
			local response = e.response

			if dataType == "text/plain" then
				handler(response)
			elseif dataType == "application/json" or dataType == "json" then
				response = json.decode(response)

				handler(response)
			end
		end, params)
end

return M