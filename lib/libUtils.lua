-----------------------------------------------
--
--  libUtils.lua
--
-----------------------------------------------
--  Global utilities

--[====[
	@type func
	@name name print_r(t)
	@param t:table Table that need pring
	@brief Displays the table structure
--]====]
function print_r(t) 
	local print_r_cache={}

	local function sub_print_r(t,indent)
		if (print_r_cache[tostring(t)]) then
			print(indent.."*"..tostring(t))
		else
			print_r_cache[tostring(t)]=true

			if (type(t)=="table") then
				for pos,val in pairs(t) do
					if (type(val)=="table") then
						print(indent.."["..pos.."] => "..tostring(t).." {")
						sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
						print(indent..string.rep(" ",string.len(pos)+6).."}")
					elseif (type(val)=="string") then
						print(indent.."["..pos..'] => "'..val..'"')
					else
						print(indent.."["..pos.."] => "..tostring(val))
					end
				end
			else
				print(indent..tostring(t))
			end
		end
	end

	if (type(t)=="table") then
		print(tostring(t).." {")
		sub_print_r(t,"  ")
		print("}")
	else
		sub_print_r(t,"  ")
	end

	print()
end

--[====[
	@type func
	@name name isset(obj, key)
	@param obj:table Input table
	@param key:any Input table key
	@return bool <b>TRUE</b> if value is set
	@brief Check if this value is set by key
--]====]
function isset(obj, key)
	if type(obj) ~= "table" then
		return false
	end

	return not empty(obj[key])
end

--[====[
	@type func
	@name name empty(obj)
	@param obj:any Input value
	@return bool <b>TRUE</b> if value is empty
	@brief Check if this value is empty
--]====]
function empty(obj)
	local result = false

	if obj == nil or obj == "" then
		return true
	end

	local objType = type(obj)
	
	if objType == "table" and count(obj) == 0 then
		return true
	end

	return false
end

--[====[
	@type func
	@name count(obj)
	@param obj:table Input table
	@return number/nil Number of elements
	@brief Calculate the exact number of elements in array. Return <b>nil</b> if input <i>obj</i> is not table
--]====]
function count(obj)
	if type(obj) ~= "table" then
		return nil
	end

	local count = 0

	for k, v in pairs(obj) do
	   count = count + 1 
	end

	return count
end

--[====[
	@type func
	@name getItem(obj, key, default?)
	@param obj:table Input table
	@param key:string Table key
	@param default:any Optional. Default value
	@return any Searched value
	@brief Return value by key, if value by key don't exist, than return <b>default</b> value, if <b>default</b> value don't set, than return <b>nil</b>
--]====]
function getItem(obj, key, default)
	if type(obj) ~= "table" then
		return nil
	end

	if empty(obj[key]) then
		if empty(default) then
			return nil
		end

		return default
	end

	return obj[key]
end

--[====[
	@type func
	@name isString(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if string, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isString(obj)
	return type(obj) == "string"
end

--[====[
	@type func
	@name isNil(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if nil, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isNil(obj)
	return type(obj) == "nil"
end

--[====[
	@type func
	@name isNil(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if table, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isTable(obj)
	return type(obj) == "table"
end

--[====[
	@type func
	@name isNumber(obj)
	@param obj:any Any input value
	@return bool <b>TRUE</b> if number, <b>FALSE</b> in another way
	@brief Check value type
--]====]
function isNumber(obj)
	return type(obj) == "number"
end
